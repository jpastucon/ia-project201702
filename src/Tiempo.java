
import java.util.TimerTask;

/**
 *
 * @author jpastucon
 */
public class Tiempo extends TimerTask {

    private int tiempo = 0;

    @Override
    public void run() {
        tiempo++;

    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

}
