
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

/**
 *
 * @author jpastucon
 */
public class Panel_Datos extends TimerTask implements Constantes {

    public Lienzo lienzo;
    public DefaultTableModel modelo;
    public JTable tabla;
    public Celda celda_aux;
    public double distanciaCelda_RJ;
    JScrollPane scroll;
    FileOutputStream os;

    String[] columnNames = {"DJA1", "DJA2", "DJA3", "PJA1", "PJA2", "PJA3", "EJ", "DJR*", "DA1R*", "DA2R*", "DA3R*"};
    public Object[][] data = {};

    public Panel_Datos(Lienzo lienzo) throws FileNotFoundException {
        os = new FileOutputStream("traza.txt");
        this.lienzo = lienzo;
        scroll = new JScrollPane();
        modelo = new DefaultTableModel(data, columnNames);
        tabla = new JTable(modelo);
        TableColumn column = null;
        for (int i = 0; i < 10; i++) {
            switch (i) {
                case 0:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(60);
                    break;
                case 1:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(60);
                    break;
                case 2:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(60);
                    break;
                case 3:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(40);
                    break;
                case 4:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(40);
                    break;
                case 5:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(40);
                    break;
                case 6:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(50);
                    break;
                case 7:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(70);
                    break;
                case 8:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(70);
                    break;
                case 9:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(70);
                    break;
                case 10:
                    tabla.getColumnModel().getColumn(i).setPreferredWidth(70);
                    break;
            }
        }
        distanciaCelda_RJ = 20;
        celda_aux = new Celda(0, 0, 'R');
        scroll.setViewportView(tabla);
    }

    @Override
    public void run() {

        double DJA1 = Distancia_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario1);
        double DJA2 = Distancia_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario2);
        double DJA3 = Distancia_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario3);
        int PJA1 = Proteccion_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario1);
        int PJA2 = Proteccion_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario2);
        int PJA3 = Proteccion_Jugador_Adversario(lienzo.escenario.jugador, lienzo.escenario.adversario3);
        int EJ = lienzo.escenario.jugador.energia;
        double DJR = Verifica_RecompensaMasCercana_Jugador();
        double DA1R = Distancia(lienzo.escenario.adversario1.posicionX, lienzo.escenario.adversario1.posicionY, celda_aux.x, celda_aux.y);
        double DA2R = Distancia(lienzo.escenario.adversario2.posicionX, lienzo.escenario.adversario2.posicionY, celda_aux.x, celda_aux.y);
        double DA3R = Distancia(lienzo.escenario.adversario3.posicionX, lienzo.escenario.adversario3.posicionY, celda_aux.x, celda_aux.y);

//        System.out.print("DJA1: " + DJA1);
//        System.out.print(" DJA2: " + DJA2);
//        System.out.println(" DJA3: " + DJA3);
//        System.out.print("PJA1: " + PJA1);
//        System.out.print(" PJA2: " + PJA2);
//        System.out.println(" PJA3: " + PJA3);
//        System.out.println("EJ: " + EJ);
        Object[] newRow = {DJA1, DJA2, DJA3, PJA1, PJA2, PJA3, EJ, DJR, DA1R, DA2R, DA3R};

        modelo.addRow(newRow);
        PrintStream ps = new PrintStream(os);
        ps.println(DJA1 + ", " + DJA2 + ", " + DJA3 + ", " + PJA1 + ", " + PJA2 + ", " + PJA3 + ", " + EJ + ", " + DJR + ", " + DA1R + "," + DA2R + ", " + DA3R);

    }

    public double Distancia_Jugador_Adversario(Jugador jugador, Adversario adversario) {
        double x1 = jugador.posicionX;
        double y1 = jugador.posicionY;
        double x2 = adversario.posicionX;
        double y2 = adversario.posicionY;

        double valor;
        double parte1=Math.pow(x2-x1, 2);
        double parte2=Math.pow(y2-y1, 2);
        parte1+=parte2;
        valor = Math.sqrt(parte1);
        return valor;
    }

    public int Proteccion_Jugador_Adversario(Jugador jugador, Adversario adversario) {
        int ct_muros = 0;
        int Jx = jugador.posicionX;
        int Ax = adversario.posicionX;
        int Jy = jugador.posicionY;
        int Ay = adversario.posicionY;
        if (Jx <= Ax && Jy < Ay) {
            for (int i = Jy; i <= Ay; i++) {
                for (int j = Jx; j <= Ax; j++) {
                    if (lienzo.escenario.celdas[j][i].tipo == 'O') {
                        ct_muros = ct_muros + 1;
                    }
                }
            }

        }

        if (Ax <= Jx && Ay < Jy) {
            for (int i = Ay; i <= Jy; i++) {
                for (int j = Ax; j <= Jx; j++) {
                    if (lienzo.escenario.celdas[j][i].tipo == 'O') {
                        ct_muros = ct_muros + 1;
                    }
                }
            }
        }

        if (Ax < Jx && Ay >= Jy) {
            for (int i = Jy; i <= Ay; i++) {
                for (int j = Ax; j <= Jx; j++) {
                    if (lienzo.escenario.celdas[j][i].tipo == 'O') {
                        ct_muros = ct_muros + 1;
                    }
                }
            }
        }

        if (Ax > Jx && Ay <= Jy) {
            for (int i = Ay; i <= Jy; i++) {
                for (int j = Jx; j <= Ax; j++) {
                    if (lienzo.escenario.celdas[j][i].tipo == 'O') {
                        ct_muros = ct_muros + 1;
                    }
                }
            }
        }
        return ct_muros;
    }

    public double Distancia(double x1, double y1, double x2, double y2) {
        double valor;
        double parte1=Math.pow(x2-x1, 2);
        double parte2=Math.pow(y2-y1, 2);
        parte1+=parte2;
        valor = Math.sqrt(parte1);
        return valor;
    }
    
    public double Verifica_RecompensaMasCercana_Jugador() {
        double max = 0;
        double dJR1 = Distancia(lienzo.escenario.jugador.posicionX, lienzo.escenario.jugador.posicionY, 4, 5);
        double dJR2 = Distancia(lienzo.escenario.jugador.posicionX, lienzo.escenario.jugador.posicionY, 17, 1);
        double dJR3 = Distancia(lienzo.escenario.jugador.posicionX, lienzo.escenario.jugador.posicionY, 1, 12);
        PriorityQueue q = new PriorityQueue();
        q.add(dJR1);
        q.add(dJR2);
        q.add(dJR3);
//        System.out.println(q);
//        double min = (double) q.peek();
//        System.out.println(min);

//        System.out.println(celda_aux.x+"\t"+celda_aux.y);
        return (double) q.peek();
    }

}
