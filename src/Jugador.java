
import java.util.Random;
import java.util.TimerTask;

/**
 *
 * @author jpastucon
 */
public class Jugador implements Constantes {
public int posicionX,posicionY;
    public Escenario escenario;
    public BusquedaAnchuraJugador inteligencia;
    public int energia, contadorRecompensas;
     
    public Jugador(Escenario escenario){
        this.escenario=escenario;
        inteligencia=new BusquedaAnchuraJugador(escenario, this);
        posicionX=9;
        posicionY=18;
        energia=25;
        contadorRecompensas = 0;
        escenario.celdas[posicionX][posicionY].tipo='J';
    }
    
    public void moverArriba(){
        if (posicionY > 0 ) {
           if(escenario.celdas[posicionX][posicionY-1].tipo=='R'){
                escenario.celdas[posicionX][posicionY].tipo='M';
                posicionY=posicionY-1;
                energia+=10;
                contadorRecompensas+=1;
                escenario.celdas[posicionX][posicionY].tipo='J';  
            }else if(escenario.celdas[posicionX][posicionY-1].tipo!='O'
                    && escenario.celdas[posicionX][posicionY-1].tipo!='A'
                    && escenario.celdas[posicionX][posicionY-1].tipo!='P'){
                escenario.celdas[posicionX][posicionY].tipo='M';
                posicionY=posicionY-1;
                escenario.celdas[posicionX][posicionY].tipo='J';   
            }
        }
    }
    
    public void moverAbajo(){
       if (posicionY+1 < NUM_CELDAS_LARGO ) {
           if(escenario.celdas[posicionX][posicionY+1].tipo=='R'){
                escenario.celdas[posicionX][posicionY].tipo='M';                
                posicionY=posicionY+1;   
                energia+=10;
                contadorRecompensas+=1;
                escenario.celdas[posicionX][posicionY].tipo='J';
               
            }else if(escenario.celdas[posicionX][posicionY+1].tipo!='O'
                    && escenario.celdas[posicionX][posicionY+1].tipo!='A'
                    && escenario.celdas[posicionX][posicionY+1].tipo!='P'){
                escenario.celdas[posicionX][posicionY].tipo='M';
                posicionY=posicionY+1;
                escenario.celdas[posicionX][posicionY].tipo='J';   
            }
        }
    }
    
    public void moverIzquierda(){
        if (posicionX > 0 ) {
            if(escenario.celdas[posicionX-1][posicionY].tipo=='R'){
                escenario.celdas[posicionX][posicionY].tipo='M';                
                posicionX=posicionX-1;
                energia+=10;
                contadorRecompensas+=1;                
                escenario.celdas[posicionX][posicionY].tipo='J';               
            }else if(escenario.celdas[posicionX-1][posicionY].tipo!='O'
                    && escenario.celdas[posicionX-1][posicionY].tipo!='A'
                    && escenario.celdas[posicionX-1][posicionY].tipo!='P'){
                escenario.celdas[posicionX][posicionY].tipo='M';
                posicionX=posicionX-1;
                escenario.celdas[posicionX][posicionY].tipo='J';   
            }
        }
    }
    
    public void moverDerecha(){
        if (posicionX+1 < NUM_CELDAS_ANCHO ) {
           if(escenario.celdas[posicionX+1][posicionY].tipo=='R'){
                escenario.celdas[posicionX][posicionY].tipo='M';                
                posicionX=posicionX+1;
                energia+=10;
                contadorRecompensas+=1;
                escenario.celdas[posicionX][posicionY].tipo='J';                
            }else if(escenario.celdas[posicionX+1][posicionY].tipo!='O'
                    && escenario.celdas[posicionX+1][posicionY].tipo!='A'
                    && escenario.celdas[posicionX+1][posicionY].tipo!='P'){
                escenario.celdas[posicionX][posicionY].tipo='M';
                posicionX=posicionX+1;
                escenario.celdas[posicionX][posicionY].tipo='J';    
            }   
        }
    }
}
