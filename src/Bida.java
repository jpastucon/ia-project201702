
import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 *
 * @author jpastucon
 */
public class Bida extends TimerTask {

    public Escenario escenario;
    public int flag;

    public Bida(Escenario escenario) {
        this.escenario = escenario;
        flag = 0;
    }

    @Override
    public void run() {
        if (escenario.jugador.contadorRecompensas >= 3) {
            cerrarHilos();
            if (flag == 0) {
                JOptionPane.showMessageDialog(null, "👍 GANASTE 👍", "Felicidades", JOptionPane.PLAIN_MESSAGE);
                flag = 1;
            }
            //System.exit(0);
        }
        if (escenario.jugador.energia >= 1) {
            escenario.jugador.energia--;
        } else {
            cerrarHilos();
            if (flag == 0) {
                JOptionPane.showMessageDialog(null, "👎 PERDISTE 👎", "Game Over", JOptionPane.PLAIN_MESSAGE);
                flag = 1;
            }
            //System.exit(0);
        }
    }

    public void cerrarHilos() {
        escenario.jugador.inteligencia.cancel();
        escenario.adversario1.inteligencia.cancel();
        escenario.adversario2.inteligencia.cancel();
        escenario.adversario3.inteligencia.cancel();
        escenario.lienzo.panel.cancel();
        escenario.lienzo.lanzadorTareas.cancel();
        escenario.recorredor.cancel();
        escenario.paco.inteligencia.cancel();
    }

}
