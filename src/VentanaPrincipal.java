
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.FileNotFoundException;
import java.util.Timer;
import javax.swing.JFrame;

/**
 *
 * @author jpastucon
 */
public class VentanaPrincipal extends JFrame implements Constantes {

    public Lienzo lienzo;
       

    public VentanaPrincipal() throws FileNotFoundException {
        
        lienzo = new Lienzo();
        lienzo.setFocusable(true);
        lienzo.requestFocus();
        

        this.getContentPane().setLayout(new GridLayout(1,2));
        this.getContentPane().add(lienzo);
        this.getContentPane().add(lienzo.panel.scroll);
        this.setSize((int) width, (int) height);

        

    }

    public static void main(String[] args) throws FileNotFoundException {
        VentanaPrincipal ventana = new VentanaPrincipal();
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
