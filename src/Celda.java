
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author jpastucon
 */
public class Celda extends JComponent implements Constantes {

    public int x;
    public int y;
    public char tipo;
    public boolean flag;
    public double calidad;

    public Celda(int x, int y, char tipo) {
        this.x = x;
        this.y = y;
        this.tipo = tipo;
        calidad = 0;
    }

    public void esPared() {
        tipo = 'O';
    }

    public void esRecompensa() {
        tipo = 'R';
    }

    public void esFinal() {
        tipo = 'F';
    }

    public void esJugador() {
        tipo = 'J';
    }

    public void esMapa() {
        tipo = 'M';
    }

    public void esAdversario() {
        tipo = 'A';
    }

    public void esPaco() {
        tipo = 'P';
    }
    
    @Override
    public void update(Graphics g) {
        switch (tipo) {
            case 'J':
                g.setColor(COLOR_JUGADOR);
                break;
            case 'M':
                g.setColor(COLOR_MAPA);
                break;
            case 'R':
                g.setColor(COLOR_RECOMPENSA);
                break;
            case 'F':
                g.setColor(COLOR_FINAL);
                break;
            case 'O':
                g.setColor(COLOR_OBSTACULO);
                break;
            case 'A':
                g.setColor(COLOR_ADVERSARIO);
                break;
            case 'P':
                g.setColor(COLOR_PACO);
                break;

        }
        g.fillRect(x, y, PIXEL_CELDA, PIXEL_CELDA);

        if (tipo == 'J') {
            g.setColor(Color.BLACK);
            g.drawString("J", x + 15, y + 20);
        }
        if (tipo == 'A') {
            g.setColor(Color.BLACK);
            g.drawString("A", x + 13, y + 20);
        }
        if (tipo == 'R') {
            g.setColor(Color.BLACK);
            g.drawString("R", x + 13, y + 20);
        }
        if (tipo == 'F') {
            g.setColor(Color.BLACK);
            g.drawString("F", x + 13, y + 20);
        }
        if (tipo == 'P') {
            g.setColor(Color.BLACK);
            g.drawString("P", x + 13, y + 20);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }

}
