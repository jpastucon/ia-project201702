import java.util.TimerTask;
/**
 *
 * @author jpastucon
 */
public class recorredorEscenario extends TimerTask implements Constantes {

    public Escenario escenario;
    
    public recorredorEscenario(Escenario escenario){
        this.escenario=escenario;
                    
    }
    
    public void inicializarMatrizCalidades(){
        for(int i=0;i<NUM_CELDAS_ANCHO;i++){
            for(int j=0;j<NUM_CELDAS_LARGO;j++){
                escenario.celdas[i][j].calidad=0;}
        }
    }  

     public void rango(){
        for(int i=0;i<NUM_CELDAS_ANCHO;i++){
            for(int j=0;j<NUM_CELDAS_LARGO;j++){
                Estado jug = new Estado(i,j,'N',null);
                Estado adve=new Estado(escenario.adversario1.posicionX,escenario.adversario1.posicionY,'N',null);
                jug.distancia(adve);
                
                Estado jug1 = new Estado(i,j,'N',null);
                Estado adve1=new Estado(escenario.adversario2.posicionX,escenario.adversario2.posicionY,'N',null);
                jug1.distancia(adve1);
                
                Estado jug2 = new Estado(i,j,'N',null);
                Estado adve2=new Estado(escenario.adversario3.posicionX,escenario.adversario3.posicionY,'N',null);
                jug2.distancia(adve2);
                
                double temp=jug.calidad+jug1.calidad+jug2.calidad;
                temp=temp*-1;
                escenario.celdas[i][j].calidad=temp;      
            }
        }
    }
            
    @Override
    public void  run() {
        inicializarMatrizCalidades();
        rango();    }
        }
    
        
