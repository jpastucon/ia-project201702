
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Random;

/**
 *
 * @author jpastucon
 */
public interface Constantes {

    public final int PIXEL_CELDA = 32;

    public Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    double width = screenSize.getWidth();
    double height = screenSize.getHeight();

    public final int NUM_CELDAS_ANCHO = 20;
    public final int NUM_CELDAS_LARGO = 20;

    public final int ANCHO_BORDE_VENTANA = 54;
    public final int LARGO_BORDE_VENTANA = 50;

    public final int ANCHO_ESCENARIO = (PIXEL_CELDA * NUM_CELDAS_ANCHO) + ANCHO_BORDE_VENTANA;
    public final int LARGO_ESCENARIO = (PIXEL_CELDA * NUM_CELDAS_LARGO) + LARGO_BORDE_VENTANA;

    public final char JUGADOR = 'J';
    public final char MAPA = 'M';
    public final char OBSTACULO = 'O';
    public final char ADVERSARIO = 'A';
    public final char RECOMPENSA = 'R';
    public final char FINAL = 'F';
    public final char PACO = 'P';

    public final Color COLOR_JUGADOR = Color.GREEN;
    public final Color COLOR_MAPA = Color.WHITE;
    public final Color COLOR_OBSTACULO = Color.BLACK;
    public final Color COLOR_ADVERSARIO = Color.RED;
    public final Color COLOR_RECOMPENSA = Color.ORANGE;
    public final Color COLOR_FINAL = Color.MAGENTA;
    public final Color COLOR_PACO = Color.BLUE;

    default int numeroAleatorio(int minimo, int maximo) {
        Random random = new Random();
        int numero_aleatorio = random.nextInt((maximo - minimo) + 1) + minimo;
        return numero_aleatorio;

    }

}
