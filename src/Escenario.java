
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;

/**
 *
 * @author jpastucon
 */
public class Escenario extends JComponent implements Constantes {

    public Celda[][] celdas;
    public Jugador jugador;
    public Adversario adversario1, adversario2, adversario3;
    public Paco paco;
    public Bida vida;
    public recorredorEscenario recorredor;
    public Lienzo lienzo;
    public int tiempo;    
    
    public Escenario(Lienzo lienzo) {
        this.lienzo = lienzo;
        inicializarMapa();
        inicializarParedes();
        inicializarJugador();
        inicializarRecompensa();
        inicializarAdversarios();
        paco = new Paco(this,4,3);
        celdas[4][3].tipo='P';
        tiempo = 0;
        vida=new Bida(this);
        recorredor=new recorredorEscenario(this);
    }

    public void inicializarMapa() {
        celdas = new Celda[NUM_CELDAS_ANCHO][NUM_CELDAS_LARGO];

        for (int i = 0; i < NUM_CELDAS_ANCHO; i++) {
            for (int j = 0; j < NUM_CELDAS_LARGO; j++) {
                celdas[i][j] = new Celda(i + (i * PIXEL_CELDA), j + (j * PIXEL_CELDA), 'M');
                
            }
        }
    }

    public void inicializarParedes() {
        for (int i = 0; i < NUM_CELDAS_ANCHO; i++) {
            for (int j = 0; j < NUM_CELDAS_LARGO; j++) {
                if (i == 0 || j == 0 || i == NUM_CELDAS_ANCHO - 1 || j == NUM_CELDAS_LARGO - 1) {
                    celdas[i][j].esPared();
                }
            }
        }
        
        
        celdas[2][2].esPared();
        celdas[3][2].esPared();
        celdas[5][2].esPared();
        celdas[6][2].esPared();
//        celdas[7][2].esPared();
        celdas[9][1].esPared();
        celdas[9][2].esPared();
        celdas[11][2].esPared();
        celdas[12][2].esPared();
        celdas[13][2].esPared();
        celdas[15][2].esPared();
        celdas[16][2].esPared();
        
        celdas[2][4].esPared();
        celdas[3][4].esPared();
        celdas[5][4].esPared();
        celdas[7][4].esPared();
        celdas[8][4].esPared();
        celdas[9][4].esPared();
        celdas[10][4].esPared();
        celdas[11][4].esPared();
        celdas[13][4].esPared();
        celdas[15][4].esPared();
        celdas[16][4].esPared();
        
        celdas[5][5].esPared();
        celdas[9][5].esPared();
        celdas[13][5].esPared();
        
        celdas[1][6].esPared();
        celdas[2][6].esPared();
        celdas[3][6].esPared();
        celdas[5][6].esPared();
        celdas[6][6].esPared();
        celdas[7][6].esPared();
        celdas[9][6].esPared();
        celdas[11][6].esPared();
        celdas[12][6].esPared();
        celdas[13][6].esPared();
        celdas[15][6].esPared();
//        celdas[16][6].esPared();
        celdas[17][6].esPared();
        
        celdas[1][7].esPared();
        celdas[2][7].esPared();
        celdas[3][7].esPared();
        celdas[5][7].esPared();
        celdas[13][7].esPared();
        celdas[15][7].esPared();
        celdas[16][7].esPared();
        celdas[17][7].esPared();
        
        celdas[7][8].esPared();
        celdas[11][8].esPared();
                
        celdas[5][9].esPared();
        celdas[7][9].esPared();
        celdas[8][9].esPared();
        celdas[9][9].esPared();
//        celdas[10][9].esPared();
        celdas[11][9].esPared();
        celdas[13][9].esPared();
        
        celdas[1][10].esPared();
        celdas[2][10].esPared();
        celdas[3][10].esPared();
        celdas[5][10].esPared();
        celdas[13][10].esPared();
        celdas[15][10].esPared();
        celdas[16][10].esPared();
        celdas[17][10].esPared();
        
        celdas[1][11].esPared();
        celdas[2][11].esPared();
        celdas[3][11].esPared();
        celdas[5][11].esPared();
        celdas[6][11].esPared();
        celdas[7][11].esPared();
        celdas[9][11].esPared();
        celdas[11][11].esPared();
        celdas[12][11].esPared();
        celdas[13][11].esPared();
        celdas[15][11].esPared();
        celdas[16][11].esPared();
        celdas[17][11].esPared();
        
        celdas[9][12].esPared();
        
        celdas[2][13].esPared();
        celdas[3][13].esPared();
        celdas[5][13].esPared();
        celdas[6][13].esPared();
        celdas[7][13].esPared();
        celdas[9][13].esPared();
        celdas[11][13].esPared();
        celdas[12][13].esPared();
        celdas[13][13].esPared();
        celdas[15][13].esPared();
        celdas[16][13].esPared();
        
        celdas[3][14].esPared();
        celdas[15][14].esPared();
        
        celdas[1][15].esPared();
        celdas[3][15].esPared();
        celdas[5][15].esPared();
        celdas[7][15].esPared();
        celdas[8][15].esPared();
        celdas[9][15].esPared();
        celdas[10][15].esPared();
        celdas[11][15].esPared();
        celdas[13][15].esPared();
        celdas[15][15].esPared();
        celdas[17][15].esPared();
        
        celdas[5][16].esPared();
        celdas[9][16].esPared();
        celdas[13][16].esPared();
        
        celdas[2][17].esPared();
        celdas[3][17].esPared();
        celdas[4][17].esPared();
        celdas[5][17].esPared();
        celdas[6][17].esPared();
        celdas[7][17].esPared();
        celdas[9][17].esPared();
        celdas[11][17].esPared();
        celdas[12][17].esPared();
        celdas[13][17].esPared();
        celdas[14][17].esPared();
        celdas[15][17].esPared();
        celdas[16][17].esPared();
        
    }

    public void inicializarJugador() {
        jugador = new Jugador(this);
        celdas[9][18].tipo = 'J';
    }

    public void inicializarRecompensa() {

        celdas[4][5].esRecompensa();
        celdas[17][1].esRecompensa();
        celdas[1][12].esRecompensa();
    }

    public void inicializarAdversarios() {
        adversario1 = new Adversario(this, 8, 8);
        celdas[8][8].tipo = 'A';
        adversario2 = new Adversario(this, 9, 8);
        celdas[9][8].tipo = 'A';
        adversario3 = new Adversario(this, 10, 8);
        celdas[10][8].tipo = 'A';
    }

    public void moverJugador(KeyEvent evt) {

        switch (evt.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                jugador.moverAbajo();
                break;
            case KeyEvent.VK_UP:
                jugador.moverArriba();
                break;
            case KeyEvent.VK_RIGHT:
                jugador.moverDerecha();
                break;
            case KeyEvent.VK_LEFT:
                jugador.moverIzquierda();
                break;

        }

    }

    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }

    
    @Override
    public void update(Graphics g){
        for (int i = 0; i < NUM_CELDAS_ANCHO; i++) {
            for (int j = 0; j < NUM_CELDAS_LARGO; j++) {
                celdas[i][j].paintComponent(g);
                g.setColor(Color.BLACK);
                g.drawString(""+(int)celdas[i][j].calidad, i*PIXEL_CELDA+16,j*PIXEL_CELDA+8);
            }
        }
    }
    
    
    
//    public void moverAdversario(KeyEvent evt) {
//
//        switch (evt.getKeyCode()) {
//            case KeyEvent.VK_DOWN:
//                adversario1.moverAbajo();
//                break;
//            case KeyEvent.VK_UP:
//                adversario1.moverArriba();
//                break;
//            case KeyEvent.VK_RIGHT:
//                adversario1.moverDerecha();
//                break;
//            case KeyEvent.VK_LEFT:
//                adversario1.moverIzquierda();
//                break;
//
//        }
//    }
}
