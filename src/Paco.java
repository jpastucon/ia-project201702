
/**
 *
 * @author jpastucon
 */
public class Paco implements Constantes {

    public int posicionX;
    public int posicionY;
    public Escenario escenario;
    public BusquedaAnchuraPaco inteligencia;

    public Paco(Escenario escenario, int x, int y) {
        posicionX = x;
        posicionY = y;
        this.escenario = escenario;
        inteligencia = new BusquedaAnchuraPaco(escenario, this);
        escenario.celdas[x][y].tipo = 'P';

    }

    public void moverArriba() {
        if (posicionY > 0) {
            if (escenario.celdas[posicionX][posicionY - 1].tipo == 'A') {
                if (posicionX == escenario.adversario1.posicionX && posicionY - 1 == escenario.adversario1.posicionY) {
                    escenario.adversario1.inteligencia.estadoInteligencia = 0;
                    escenario.adversario1.inteligencia.cancel();

                }
                if (posicionX == escenario.adversario2.posicionX && posicionY - 1 == escenario.adversario2.posicionY) {
                    escenario.adversario2.inteligencia.estadoInteligencia = 0;
                    escenario.adversario2.inteligencia.cancel();

                }
                if (posicionX == escenario.adversario3.posicionX && posicionY - 1 == escenario.adversario3.posicionY) {
                    escenario.adversario3.inteligencia.estadoInteligencia = 0;
                    escenario.adversario3.inteligencia.cancel();

                }
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';

//                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.jugador.inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//                escenario.vida.cerrarHilos();
            } else if (escenario.celdas[posicionX][posicionY - 1].tipo != 'O'
                    && escenario.celdas[posicionX][posicionY - 1].tipo != 'J'
                    && escenario.celdas[posicionX][posicionY - 1].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';

            }
        }
    }

    public void moverAbajo() {
        if (posicionY + 1 < NUM_CELDAS_LARGO) {
            if (escenario.celdas[posicionX][posicionY + 1].tipo == 'A') {
                if (posicionX == escenario.adversario1.posicionX && posicionY + 1 == escenario.adversario1.posicionY) {
                    escenario.adversario1.inteligencia.estadoInteligencia = 0;
                    escenario.adversario1.inteligencia.cancel();
                }
                if (posicionX == escenario.adversario2.posicionX && posicionY + 1 == escenario.adversario2.posicionY) {
                    escenario.adversario2.inteligencia.estadoInteligencia = 0;
                    escenario.adversario2.inteligencia.cancel();
                }
                if (posicionX == escenario.adversario3.posicionX && posicionY + 1 == escenario.adversario3.posicionY) {
                    escenario.adversario3.inteligencia.estadoInteligencia = 0;
                    escenario.adversario3.inteligencia.cancel();
                }
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
//                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.vida.cancel();
//                escenario.jugador.inteligencia.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//                escenario.vida.cerrarHilos();
            } else if (escenario.celdas[posicionX][posicionY + 1].tipo != 'O'
                    && escenario.celdas[posicionX][posicionY + 1].tipo != 'J'
                    && escenario.celdas[posicionX][posicionY + 1].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
            }
        }
    }

    public void moverIzquierda() {
        if (posicionX > 0) {
            if (escenario.celdas[posicionX - 1][posicionY].tipo == 'A') {
                if (posicionX - 1 == escenario.adversario1.posicionX && posicionY == escenario.adversario1.posicionY) {
                    escenario.adversario1.inteligencia.estadoInteligencia = 0;
                    escenario.adversario1.inteligencia.cancel();
                }
                if (posicionX - 1 == escenario.adversario2.posicionX && posicionY == escenario.adversario2.posicionY) {
                    escenario.adversario2.inteligencia.estadoInteligencia = 0;
                    escenario.adversario2.inteligencia.cancel();
                }
                if (posicionX - 1 == escenario.adversario3.posicionX && posicionY == escenario.adversario3.posicionY) {
                    escenario.adversario3.inteligencia.estadoInteligencia = 0;
                    escenario.adversario3.inteligencia.cancel();
                }
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
//                escenario.jugador.energia -= 5;
//                escenario.jugador.inteligencia.cancel();
//                inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//                  escenario.vida.cerrarHilos();
            } else if (escenario.celdas[posicionX - 1][posicionY].tipo != 'O'
                    && escenario.celdas[posicionX - 1][posicionY].tipo != 'J'
                    && escenario.celdas[posicionX - 1][posicionY].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
            }
        }
    }

    public void moverDerecha() {
        if (posicionX + 1 < NUM_CELDAS_ANCHO) {
            if (escenario.celdas[posicionX + 1][posicionY].tipo == 'A') {
                if (posicionX + 1 == escenario.adversario1.posicionX && posicionY == escenario.adversario1.posicionY) {
                    escenario.adversario1.inteligencia.estadoInteligencia = 0;
                    escenario.adversario1.inteligencia.cancel();
                    
                }
                if (posicionX + 1 == escenario.adversario2.posicionX && posicionY == escenario.adversario2.posicionY) {
                    escenario.adversario2.inteligencia.estadoInteligencia = 0;
                    escenario.adversario2.inteligencia.cancel();
                    
                }
                if (posicionX + 1 == escenario.adversario3.posicionX && posicionY == escenario.adversario3.posicionY) {
                    escenario.adversario3.inteligencia.estadoInteligencia = 0;
                    escenario.adversario3.inteligencia.cancel();
                    
                }
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
//                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.jugador.inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
            } else if (escenario.celdas[posicionX + 1][posicionY].tipo != 'O'
                    && escenario.celdas[posicionX + 1][posicionY].tipo != 'J'
                    && escenario.celdas[posicionX + 1][posicionY].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
            }
        }
    }

}
