import java.util.TimerTask;

/**
 *
 * @author jpastucon
 */
public class Cronometro extends TimerTask{
    
    public Escenario escenario;
    
    public Cronometro(Escenario escenario){
        this.escenario=escenario;
    }

    @Override
    public void run() {
        escenario.tiempo++;
        //System.out.println("Cronometro: "+ escenario.tiempo);
    }
}
