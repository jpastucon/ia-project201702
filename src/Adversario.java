
import javax.swing.JOptionPane;

public class Adversario implements Constantes {

    public int posicionX;
    public int posicionY;
    public Escenario escenario;
    public BusquedaAnchuraAdversario inteligencia;

    public Adversario(Escenario escenario, int x, int y) {
        posicionX = x;
        posicionY = y;
        this.escenario = escenario;
        inteligencia = new BusquedaAnchuraAdversario(escenario, this);
        escenario.celdas[x][y].tipo = 'A';

    }

    public void moverArriba() {
        if (posicionY > 0) {
            if (escenario.celdas[posicionX][posicionY - 1].tipo == 'P') {
                this.inteligencia.estadoInteligencia = 0;
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
                this.inteligencia.cancel();
                
            }
            if (escenario.celdas[posicionX][posicionY - 1].tipo == 'J') {
//                escenario.celdas[posicionX][posicionY].tipo='M';
//                posicionY=posicionY-1;
//                escenario.celdas[posicionX][posicionY].tipo='A';
                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.jugador.inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);

            } else if (escenario.celdas[posicionX][posicionY - 1].tipo != 'O'
                    && escenario.celdas[posicionX][posicionY - 1].tipo != 'A'
                    && escenario.celdas[posicionX][posicionY - 1].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'A';

            }
        }
    }

    public void moverAbajo() {
        if (posicionY + 1 < NUM_CELDAS_LARGO) {
            if (escenario.celdas[posicionX][posicionY + 1].tipo == 'P') {
                this.inteligencia.estadoInteligencia = 0;
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
                this.inteligencia.cancel();
                
            }
            if (escenario.celdas[posicionX][posicionY + 1].tipo == 'J') {
//                escenario.celdas[posicionX][posicionY].tipo='M';
//                posicionY=posicionY+1;
//                escenario.celdas[posicionX][posicionY].tipo='A';
                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.vida.cancel();
//                escenario.jugador.inteligencia.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//             

            } else if (escenario.celdas[posicionX][posicionY + 1].tipo != 'O'
                    && escenario.celdas[posicionX][posicionY + 1].tipo != 'A'
                    && escenario.celdas[posicionX][posicionY + 1].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionY = posicionY + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'A';
            }
        }
    }

    public void moverIzquierda() {
        if (posicionX > 0) {
            if (escenario.celdas[posicionX - 1][posicionY].tipo == 'P') {
                this.inteligencia.estadoInteligencia = 0;
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
                this.inteligencia.cancel();
                
            }
            if (escenario.celdas[posicionX - 1][posicionY].tipo == 'J') {
//                escenario.celdas[posicionX][posicionY].tipo='M';
//                posicionX=posicionX-1;
//                escenario.celdas[posicionX][posicionY].tipo='A';
                escenario.jugador.energia -= 5;
//                escenario.jugador.inteligencia.cancel();
//                inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//               
            } else if (escenario.celdas[posicionX - 1][posicionY].tipo != 'O'
                    && escenario.celdas[posicionX - 1][posicionY].tipo != 'A'
                    && escenario.celdas[posicionX - 1][posicionY].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX - 1;
                escenario.celdas[posicionX][posicionY].tipo = 'A';
            }
        }
    }

    public void moverDerecha() {
        if (posicionX + 1 < NUM_CELDAS_ANCHO) {
            if (escenario.celdas[posicionX + 1][posicionY].tipo == 'P') {
                this.inteligencia.estadoInteligencia = 0;
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'P';
                this.inteligencia.cancel();
                
            }
            if (escenario.celdas[posicionX + 1][posicionY].tipo == 'J') {
//                escenario.celdas[posicionX][posicionY].tipo='M';
//                posicionX=posicionX+1;
//                escenario.celdas[posicionX][posicionY].tipo='A';
                escenario.jugador.energia -= 5;
//                inteligencia.cancel();
//                escenario.jugador.inteligencia.cancel();
//                escenario.vida.cancel();
//                JOptionPane.showMessageDialog(null, "PERDISTE", "GAME OVER", JOptionPane.PLAIN_MESSAGE);
//                
            } else if (escenario.celdas[posicionX + 1][posicionY].tipo != 'O'
                    && escenario.celdas[posicionX + 1][posicionY].tipo != 'A'
                    && escenario.celdas[posicionX + 1][posicionY].tipo != 'R') {
                escenario.celdas[posicionX][posicionY].tipo = 'M';
                posicionX = posicionX + 1;
                escenario.celdas[posicionX][posicionY].tipo = 'A';
            }
        }
    }
}
