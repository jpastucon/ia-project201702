
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.util.Timer;

/**
 *
 * @author jpastucon
 */
public class Lienzo extends Canvas implements Constantes {

    public Escenario escenario;
    public Timer lanzadorTareas;
    public Panel_Datos panel;
    
    
    public Lienzo() throws FileNotFoundException{
        
        escenario = new Escenario(this);
        lanzadorTareas = new Timer();
        panel = new Panel_Datos(this);
        escenario=new Escenario(this);
        lanzadorHilos();
        
        MoverJugadorPorTeclado();
//        MoverJugadorPorClick();

        
    }
    
    public void lanzadorHilos(){
        lanzadorTareas.scheduleAtFixedRate(escenario.vida,0, 1000);
        lanzadorTareas.scheduleAtFixedRate(escenario.recorredor, 0, 500);
        lanzadorTareas.scheduleAtFixedRate(escenario.jugador.inteligencia,0,700);
        lanzadorTareas.scheduleAtFixedRate(escenario.adversario1.inteligencia,0,1000);
        lanzadorTareas.scheduleAtFixedRate(escenario.adversario2.inteligencia,0,1000);
        lanzadorTareas.scheduleAtFixedRate(escenario.adversario3.inteligencia,0,1000);
        lanzadorTareas.scheduleAtFixedRate(escenario.paco.inteligencia, 0, 1000);
        lanzadorTareas.scheduleAtFixedRate(panel, 0, 1000);
//        lanzadorTareas.schedule(escenario.cronometro, 0, 500);
    }
    
    private void MoverJugadorPorTeclado() {
        addKeyListener(new java.awt.event.KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent evt){
                if(escenario.jugador.energia >= 0){
                    escenario.moverJugador(evt);
                }
                //escenario.moverAdversario(evt); //Modo espejo
                repaint();
            }
        });
    }
    
//    
//    public void MoverJugadorPorClick(){
//        
//        addMouseListener(new java.awt.event.MouseAdapter() {
//            
//            @Override
//            public void mousePressed(MouseEvent evt){
//                escenario.moverJugadorPorClick(evt);
//                repaint();
//            }
//            
//            @Override
//            public void mouseReleased(MouseEvent evt){
//                escenario.moverJugadorPorClick(evt);
//                repaint();
//            }
//            
//        });
//        
//    }
//    
    @Override
    public void paint(Graphics g) {
        update(g);
    }
    
    @Override
    public void update(Graphics g){
        escenario.paintComponent(g);
        g.drawString(String.valueOf(escenario.jugador.energia), 
                escenario.celdas[escenario.jugador.posicionX][escenario.jugador.posicionY].x/*+17*/,
                 escenario.celdas[escenario.jugador.posicionX][escenario.jugador.posicionY].y+12);
    }

}
