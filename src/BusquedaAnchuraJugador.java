
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;

public class BusquedaAnchuraJugador extends TimerTask implements Constantes {

    public Escenario escenario;
    public Queue<Estado> colaEstados;
    public Queue<Estado> destinos;
    public ArrayList<Estado> historial;
    public ArrayList<Character> pasos;
    public int index_pasos;
    public Estado objetivo;
    public Estado temp;
    public boolean exito;
    public recorredorEscenario recorredor;
    public Jugador jugador;

    public BusquedaAnchuraJugador(Escenario escenario, Jugador jugador) {
        this.escenario = escenario;
        this.jugador = jugador;
        colaEstados = new PriorityQueue<>(); //COLA DE PRIORIDAD
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        exito = false;
        destinos = new PriorityQueue<>();
        recorredor = new recorredorEscenario(escenario);
    }

    private void moverArriba(Estado e) {
        if (e.y > 0) {
            if (escenario.celdas[e.x][e.y - 1].tipo != 'O' && escenario.celdas[e.x][e.y - 1].tipo != 'A') {
                Estado arriba = new Estado(e.x, e.y - 1, 'U', e);
                arriba.distancia(objetivo);
                arriba.calidad += recorredor.escenario.celdas[arriba.x][arriba.y].calidad;
                if (!historial.contains(arriba)) {
                    colaEstados.add(arriba);
                    historial.add(arriba);
                    if (arriba.equals(objetivo)) {
                        objetivo = arriba;
                        exito = true;
                    }
                }
            }
        }
    }

    private void moverAbajo(Estado e) {
        if (e.y + 1 < NUM_CELDAS_LARGO) {
            if (escenario.celdas[e.x][e.y + 1].tipo != 'O' && escenario.celdas[e.x][e.y + 1].tipo != 'A') {
                Estado abajo = new Estado(e.x, e.y + 1, 'D', e);
                abajo.distancia(objetivo);
                abajo.calidad += recorredor.escenario.celdas[abajo.x][abajo.y].calidad;
                if (!historial.contains(abajo)) {
                    colaEstados.add(abajo);
                    historial.add(abajo);
                    if (abajo.equals(objetivo)) {
                        objetivo = abajo;
                        exito = true;
                    }
                }
            }
        }
    }

    private void moverIzquierda(Estado e) {
        if (e.x > 0) {
            if (escenario.celdas[e.x - 1][e.y].tipo != 'O' && escenario.celdas[e.x - 1][e.y].tipo != 'A') {
                Estado izquierda = new Estado(e.x - 1, e.y, 'L', e);
                izquierda.distancia(objetivo);
                izquierda.calidad += recorredor.escenario.celdas[izquierda.x][izquierda.y].calidad;
                if (!historial.contains(izquierda)) {
                    colaEstados.add(izquierda);
                    historial.add(izquierda);
                    if (izquierda.equals(objetivo)) {
                        objetivo = izquierda;
                        exito = true;
                    }
                }
            }
        }
    }

    private void moverDerecha(Estado e) {
        if (e.x + 1 < NUM_CELDAS_ANCHO) {
            if (escenario.celdas[e.x + 1][e.y].tipo != 'O' && escenario.celdas[e.x + 1][e.y].tipo != 'A') {
                Estado derecha = new Estado(e.x + 1, e.y, 'R', e);
                derecha.distancia(objetivo);
                derecha.calidad += recorredor.escenario.celdas[derecha.x][derecha.y].calidad;
                if (!historial.contains(derecha)) {
                    colaEstados.add(derecha);
                    historial.add(derecha);
                    if (derecha.equals(objetivo)) {
                        objetivo = derecha;
                        exito = true;
                    }
                }
            }
        }
    }

    public boolean buscar(Estado inicial, Estado objetivo) {
        
        inicial.distancia(objetivo); //Calculamos la distancia al objetivo
        
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo = objetivo;

        if (inicial.equals(objetivo)) {
            exito = true;
        }

        while (!colaEstados.isEmpty() && !exito) {
            temp = colaEstados.poll(); //Funcion que proporciona el elemento con mayor prioridad, lo asigna a temp y lo elimina de la cola de estados
            moverArriba(temp);
            moverAbajo(temp);
            moverIzquierda(temp);
            moverDerecha(temp);
        }
        if (exito) {
            //this.calcularRuta();
            return true;
        } else {
            return false;
        }
    }

    public void calcularRuta() {
        Estado predecesor = objetivo;
        do {
            pasos.add(predecesor.oper);
            predecesor = predecesor.predecesor;
        } while (predecesor != null);
        index_pasos = pasos.size() - 1;
    }

    public char darMovimiento() {
        return pasos.get(index_pasos - 1);
    }

    public void manchar(int x1, int y1) {
        int x = x1, y = y1;
        for (x = x - 2; x < x1 + 2; x++) {
            for (y = y - 2; y < y1 + 2; y++) {
                escenario.celdas[x][y].calidad += 5;
            }
        }

    }

    public void goToEnergy() {
        destinos.clear();
        for (int i = 0; i < NUM_CELDAS_ANCHO; i++) {
            for (int j = 0; j < NUM_CELDAS_LARGO; j++) {

                if (escenario.celdas[i][j].tipo == 'R') {
                    //System.out.println("Objetivo fijado");
                    Estado recompensa = new Estado(i, j, 'N', null);
                    Estado jugadorcillo = new Estado(jugador.posicionX, jugador.posicionY, 'N', null);
                    recompensa.distancia(jugadorcillo);
                    destinos.add(recompensa);
                }

            }
        }

    }

    public void resetearCalidadCeldas() {
        for (int i = 0; i < NUM_CELDAS_ANCHO; i++) {
            for (int j = 0; j < NUM_CELDAS_LARGO; j++) {
                escenario.celdas[i][j].calidad = 0;
            }
        }
    }

    public void resetear() {
        colaEstados.clear();
        pasos.clear();
        historial.clear();
        exito = false;
    }

    @Override
    public void run() {
        resetear();
        resetearCalidadCeldas();
        Estado subinicial;
        Estado subobjetivo;
        goToEnergy();
        if(escenario.adversario1.inteligencia.estadoInteligencia == 1){
            manchar(escenario.adversario1.posicionX, escenario.adversario1.posicionY);
        }
        if(escenario.adversario2.inteligencia.estadoInteligencia == 1){
            manchar(escenario.adversario2.posicionX, escenario.adversario2.posicionY);
        }
        if(escenario.adversario3.inteligencia.estadoInteligencia == 1){
            manchar(escenario.adversario3.posicionX, escenario.adversario3.posicionY);
        }
        
        if (!destinos.isEmpty()) {
            subinicial = new Estado(jugador.posicionX, jugador.posicionY, 'N', null);
            subobjetivo = destinos.peek();

            buscar(subinicial, subobjetivo);

            calcularRuta();

            /*if(subinicial.equals(subobjetivo)){ 
                destinos.remove(subobjetivo);
                System.out.println("Un objetivo menos");
            }*/
            if (pasos.size() > 1) {
                switch (darMovimiento()) {
                    case 'D':
                        jugador.moverAbajo();
                        break;
                    case 'U':
                        jugador.moverArriba();
                        break;
                    case 'R':
                        jugador.moverDerecha();
                        break;
                    case 'L':
                        jugador.moverIzquierda();
                        break;
                }
            }
        }
        escenario.lienzo.repaint();
    }
}
